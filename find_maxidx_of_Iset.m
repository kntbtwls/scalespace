function maxidx = find_maxidx_of_Iset( clresult )
%FIND_MAXIDX_OF_ISET find the maximum frame number for each sample
% by checking the Isets of each LDS

    Nmax = size(clresult,2);
    nsamples = size(clresult{1}{1}.Iset, 2); % assuming clresult{1}{1} exists

    %% find max T for each sample
    maxidx = zeros(1, nsamples);
    for N=1:Nmax % num of LDS(hclust) or iteration(EM)
        for i=1:size(clresult{N},2) % this is not N when using the result of EM
            for s=1:nsamples
                cl = clresult{N}{i};
                %size(cl.Iset)
                if size(cl.Iset{s},1)==0, continue, end;
                tmpidx = max(cl.Iset{s}(:,2));
                if( maxidx(s) < tmpidx )
                    maxidx(s) = tmpidx;
                end
            end
        end
    end

end
