function [ genseq ] = easyARgen( A, xinit, len )

    if(size(xinit,2)==1)
        genseq=zeros(length(xinit)+1,len);
        genseq(:,1)=[xinit;1];
        for t=2:len
            genseq(:,t)=A*genseq(:,t-1);
        end
    else
        X=[xinit;ones(1,size(xinit,2))];
        genseq=A*X;
    end
    genseq=genseq(1:end-1,:);

end
