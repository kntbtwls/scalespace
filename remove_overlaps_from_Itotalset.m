function Isret = remove_overlaps_from_Itotalset( Itotalset )
%REMOVE_OVERLAPS_FROM_ITOTALSET remove inclusion and overlaps from interval set
%

    if ( isempty(Itotalset) )
        Isret = [];
        return;
    end

    % 1. sorted by b
    Itotalset = sortrows(Itotalset, 1);

    % 2. remove overlaps -> id sequence
    totallen = max(Itotalset(:,2));
    ldsseq = zeros(1, totallen);
    for k=1:size(Itotalset, 1)
        ldsseq( Itotalset(k,1):Itotalset(k,2) ) = Itotalset(k,3); %  new interval overlays older intervals
    end

    % 3. id sequence -> interval set
    Isret = [];
    b = 1;
    label = ldsseq(1);
    for t=2:totallen
        if ( label ~= ldsseq(t) ) % endofinterval
            Isret = [Isret; b t-1 label];
            b = t;
            label = ldsseq(t);
        end
    end
    Isret = [Isret; b totallen label];
    Isret = Isret( Isret(:,3)~=0, : );

end
