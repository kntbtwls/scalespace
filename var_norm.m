function [ variation_norm ] = var_norm( signal )
% nńf[^Ět[ÔˇŞĚmđvZ

frame_num = size(signal, 2);

% xyĎťĘĚmđvZ
variation = signal(:, 2:frame_num) - signal(:, 1:frame_num-1);

variation_norm = zeros(1, frame_num-1);
for i = 1:frame_num-1
    variation_norm(i) = norm(variation(i));
end

end
