clear all
seqname = 'uekiQ1' ;
% uekiQ1, uekiQ2, taharaQ2
load( sprintf( '%sX.mat' , seqname));
load( sprintf( '%sY.mat' , seqname));

%resampled_intensity =
load( sprintf( '%sint.mat' , seqname));
% if (plot_intensity)
%     figure( 'Name' , 'intensity')
%     plot(intensity(:,1), intensity(:,2));
% end
% sample周期 : 8 msec = 125fps
% totalframe = 6461;
% totalsec = 270;
% fps = totalframe / totalsec;
%
% resampled_intensity = resample(intensity, totalframe, size(intensity,1));

%fps = 23.93;
%--- 特徴点の番号 -------------------------------------------------
jaw = 1:17;
eyebrow_r = 18:22;
eyebrow_l = 23:27;
nose_line = 28:31;
nose_under = 32:36;
eye_r = 37:42;
eye_l = 43:48;
mouth = 49:66;

mouth_edge = [49,55]; % 唇の端点 (x見る)
lip_ul = [52, 58]; % 上下唇 (y見る)
eye_r_ul = [38,39,41,42]; % 右目上下
eye_l_ul = [44,45,47,48]; % 左目上下

eyebrow_inner = [22,23]; % 眉間
eyebrow_r_ul = [18,20,22]; %

%--- 平滑化 ---------------------------------------------------------
use_smoothing = true;   % 平滑化するかどうか
sigma1 = 2; % ガウシアンの標準偏差 [frame]

ws1 = 6*sigma1 + 1; % Size of the averaging window
% gaussian ( max at x=3sigma+1, x=k and x=6sigma+1 - (k-1) take same value)
%window = ones(ws,1)/ws;
window1 = exp(- ((1:ws1)' - (3*sigma1+1)).^2./(sigma1^2)); % 6sigmaが入るぐらい
window1 = window1 / sum(window1); % 窓を計算
s1AfX = convn( AfX , window1, 'same' );
s1AfY = convn( AfY , window1, 'same' );
s1Intensity = convn( intensity, window1, 'same' );
save (sprintf('%sXs.mat' , seqname), 's1AfX');
save (sprintf('%sYs.mat' , seqname), 's1AfY');
save (sprintf('%sints.mat' , seqname), 's1Intensity');
figure(1)
subplot(2,1,1);
plot(AfX)
subplot(2,1,2);
plot(s1AfX)
figure(2)
subplot(2,1,1);
plot(AfY)
subplot(2,1,2);
plot(s1AfY)
figure(3)
subplot(2,1,1);
plot(intensity)
subplot(2,1,2);
plot(s1Intensity)
