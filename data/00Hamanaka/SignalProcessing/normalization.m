%特徴点を各フレーム毎に正規化する。各フレーム毎にグラフでプロットはしない。特徴点群ＡｆＸ、ＡｆＹを得る。Fxフレーム目の基準は動画ごとに適当に決める
clear all
close all
seqname = 'uekiQ1' ;
Fx = 2296;%Fxフレーム目を基準とするuekiQ1=2296,taharaQ2=39
Sh = csvread(sprintf('%s.csv', seqname));
d = size(Sh);
nFrames = d(1);%フレーム数
nPoints = (d(2) - 1)/2;%特徴点の数
for k = 1 : nFrames
  X(k,1:nPoints) = Sh(k,2:nPoints+1);%X(ｎフレーム目、各々の特徴点)座標
  Y(k,1:nPoints) = Sh(k,nPoints+2:d(2));%X(ｎフレーム目、各々の特徴点)座標
end
az = 180;
el = 90;
Fn = 0;
for Fy = 1:nFrames
    Fn = Fn + 1;
%figure
%Fx = 39;%Fxフレーム目を基準とする
Ax = zeros(3,nPoints);%特徴点ベクトル
for k = 1 : nPoints
Ax(1,k) = X(Fx,k); 
Ax(2,k) = Y(Fx,k);
end
%figure%Fyフレーム目の特徴点の様子
Ay = zeros(3,nPoints);%特徴点ベクトル
for k = 1 : nPoints
Ay(1,k) = X(Fy,k); 
Ay(2,k) = Y(Fy,k);
end
%M1=mean(A1,2);%特徴点の重心、基準
Mx=mean(Ax(:,[34,37,40,43,46]),2);
ACx=Ax-repmat(Mx,1,nPoints);%特徴点の重心を原点にするように平行移（顔の移動がこれで正規化）
%M42=mean(A42,2);%特徴点の重心
My=mean(Ay(:,[34,37,40,43,46]),2);
ACy=Ay-repmat(My,1,nPoints);%特徴点の重心を原点にするように平行移（顔の移動がこれで正規化）
QAc = ACx(:,[34,37,40,43,46]);%表情固定点５点
QB = ACy(:,[34,37,40,43,46]);
[U S V] = svd(QB*QAc','econ');
Rm=V*U';%最小化の条件より求めた回転行列R
Scam = trace(S*V'*V)/trace(QB'*QB);%最小化の条件より求めたスケール倍
ACym = Scam*Rm*ACy;%ｋフレーム目での正規化結果
AfX(Fy,1:nPoints) = ACym(1,1:nPoints);%Fyフレーム数
AfY(Fy,1:nPoints) = ACym(2,1:nPoints);
% scatter(ACx(1,:),ACx(2,:))
% hold on
% scatter(ACy(1,:),ACy(2,:))
%scatter(ACx(1,:),ACx(2,:))
%hold on
%scatter(ACym(1,:),ACym(2,:))
%view(az, el);
%xlim([-150 150]);
%ylim([-100 200]);
%F(Fy) = getframe;
close all
%R = zeros(3);
%R = R + Rm;
end
save (sprintf('%sX.mat' , seqname), 'AfX');
save (sprintf('%sY.mat' , seqname), 'AfY');
%plot(AfX(:,[49:66]))
%plot(AfY(:,[49:66]))特徴点の変異
%plot((AfY(2:6200,[56:60])-(AfY(1:6199,[56:60]))))特徴点の差分速度