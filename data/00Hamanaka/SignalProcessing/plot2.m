clear all
close all
 
%csvdata = csvread( 'taharaQ2.csv' , 0,1);
seqname = 'taharaQ2';  
load( sprintf( '%sXs.mat' , seqname));
load( sprintf( '%sYs.mat' , seqname));
%load 'taharaQ2X.mat' % 向かって，右端から反時計回りに 49-60 (49と55が端，50-54が上唇(52が真ん中), 56-60が下唇(58が真ん中)）
%load 'taharaQ2Y.mat'
 
%%
% さいしょの口元横に開く
% ptlist = [52,58];
% figure(1)
% I = 670:850;
% subplot(211), plot(I, AfX(I,ptlist)), xlabel( 'frame' ), ylabel('x')
% subplot(212), plot(I, AfY(I,ptlist)), xlabel( 'frame' ), ylabel('y')
%  
% % 気づいたタイミング
% %I = 3800:4100;
% I = 3600:4100;
% figure(2)
% subplot(211), plot(I, AfX(I,ptlist)), xlabel( 'frame' ), ylabel('x')
% subplot(212), plot(I, AfY(I,ptlist)), xlabel( 'frame' ), ylabel('y')
mouth_edge = [49,55]; % 唇の端点 (x見る)
lip_ul = [52, 58]; % 上下唇 (y見る)  
figure( 'Name' , 'parts plot');
Np = 3;
h1 = subplot(Np, 1, 1); plot(s1AfX(:, mouth_edge)), grid;
h2 = subplot(Np, 1, 2); plot(s1AfY(:, lip_ul)), grid;
h3 = subplot(Np, 1, 3); plot(s1AfY(:, lip_ul(1))-s1AfY(:, lip_ul(2))), grid;
scrollsubplot(200, 650:5000, h1, h2, h3);


