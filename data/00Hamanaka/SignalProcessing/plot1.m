clear all
close all
 
%csvdata = csvread( 'taharaQ2.csv' , 0,1);
seqname = 'taharaQ2';
% uekiQ1, uekiQ2, taharaQ2
load( sprintf( '%sXs.mat' , seqname));
load( sprintf( '%sYs.mat' , seqname));  
%load 'taharaQ2X.mat' % 向かって，右端から反時計回りに 49-60 (49と55が端，50-54が上唇(52が真ん中), 56-60が下唇(58が真ん中)）
%load 'taharaQ2Y.mat'
 
%%
% さいしょの口元横に開く
ptlist = [52,58];
figure(1)
I = 670:850;
subplot(211), plot(I, s1AfX(I,ptlist)), xlabel( 'frame' ), ylabel('x')
subplot(212), plot(I, s1AfY(I,ptlist)), xlabel( 'frame' ), ylabel('y')
 
% 気づいたタイミング
%I = 3800:4100;
I = 3600:4100;
figure(2)
subplot(211), plot(I, s1AfX(I,ptlist)), xlabel( 'frame' ), ylabel('x')
subplot(212), plot(I, s1AfY(I,ptlist)), xlabel( 'frame' ), ylabel('y')
  
