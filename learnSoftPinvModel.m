function [M, bias, val1, val2, val3] = learnSoftPinvModel(S1,S2, upper_bound, checkLSeig, centerize, calceachrow, bayesian)
%% LEARNSOFTPINV
% ridge-regression-based constraints
% calceachrow 1: constrains u_bound based on Gershgorin's theorem for each row
% calceachrow 0: constrains u_bound based on Gershgorin's theorem for maxprow
% calceachrow -1: constrains u_bound strictly: maxabs(lambda(A)) < upper_bound
%   If you need to identify A based on maxabs(lambda(A)) <= upper_bound,
%    modify some inequalities
%
    val1 = 0;
    val2 = 0;
    val3 = 0;

    step1thres = 1.1;
    step2ratio = 0.9; % original: 1/0.8
    refineratio = 0.9;
    maxit = 100;

    maxeig = 2;

    [dim1 len] = size(S1);
    dim2 = size(S2,1);
    % Note: dim1 not always equas to dim2 (e.g., AR order >=2)
    mean1 = zeros(dim1,1);
    mean2 = zeros(dim2,1);
    if (centerize ) % if Bayesian, centerization is required internally
        mean1 = mean(S1, 2);
        mean2 = mean(S2, 2);
        S1 = S1 - mean1*ones(1, len);
        S2 = S2 - mean2*ones(1, size(S2, 2));
    end

    if ( norm(S1, 'fro') < 1e-16 )
        fprintf('static seq: %.4f\n', norm(S1, 'fro') );
        %M = (1 - 1/len) * eye(dim1);
        %bias = S2(:,end)/len;
        M = zeros(dim2,dim1);
        bias = mean2;
        return;
    end

    %% if checkLSeig=1, use LS solution when eig <=1
    if checkLSeig
        M = S2 * pinv(S1);
        % use [dim1 x dim1] square matrix
        maxeig= max(abs(eig( [zeros(dim1-dim2, dim2) eye(dim1-dim2); M] ))); %*
    end

    %% Softpinv-based identification


    S1S1t = S1*S1';
    S2S1t = S2*S1';

    if(bayesian == 1)
        %% Bayesian estimation
        [U evYY] = eig(S1S1t);
        lambda = diag(evYY);
        if(calceachrow == 1)
            alpha = 1;
            beta = ones(dim2,1);
            gamma = zeros(dim2,1);
            Gamma = 0;
            delta = zeros(dim2,1);

            for it=1:maxit
                for i=1:dim2
                    delta(i) = alpha / beta(i);  % delta(i) = alpha/beta(i)  (i=1..dim2 )
                                                 %M(i,:) = S2S1t(i,:) * inv(S1S1t + delta(i)*eye(dim1));
                    M(i,:) = S2S1t(i,:) * U*diag( 1./(delta(i) + lambda ) )*U';
                    gamma(i) = sum( lambda ./ (delta(i) + lambda) );
                    beta(i) = (len-gamma(i)) / norm( S2(i,:) - M(i,:)*S1, 2)^2;
                    if(beta(i) > 100)
                        beta(i) = 100;
                    end
                end
                prev_Gamma = Gamma;
                Gamma = sum(gamma);
                if(it>1)
                    if(abs(prev_Gamma - Gamma) < 1e-2)
                        break;
                    end
                end
                alpha = Gamma / norm(M, 'fro')^2;
            end
            val1 = alpha;
            val2 = mean(beta);
            val3 = Gamma;
        else
            %% beta 共通
            alpha = 1; % 初期値
            beta = 1; % 初期値
                      %paramlog = zeros(itmax,4);
            gamma = 0;
            for it=1:maxit
                delta = alpha/beta;
                %M = S2S1t * inv(S1S1t + delta*eye(dim1)); % invの再計算をさけるために Uを使うべき
                evalinv = 1./ (delta + lambda );
                if(sum(isinf(evalinv)) || sum(isnan(evalinv))) % div by zero
                    delta = 1e-8;
                    evalinv = 1./ (delta + lambda );
                end
                M = S2S1t * U*diag(evalinv)*U'; %
                prev_gamma = gamma;
                gamma =  sum( lambda ./ (delta + lambda) ); % effective num
                if(it>1)
                    if(abs(prev_gamma - gamma) < 1e-2)
                        break;
                    end
                end
                %paramlog(it,:) = [alpha, beta, gamma, delta];
                alpha = (dim2*gamma)/norm(M, 'fro')^2;
                beta = (dim2*(len-gamma))/ norm(S2 - M*S1, 'fro')^2; % Note: No16
                if(beta > 100)
                    beta = 100;
                end
            end

            %% refinement ( shrink the size of M to satisfy maxeig<=1 )
            % 注意！できるだけ1に近くするというわけではない
            if(calceachrow<0)
                maxeig = max(abs(eig( [zeros(dim1-dim2, dim2) eye(dim1-dim2); M] ))); %*
                while (maxeig > upper_bound ) % stable ? if maxeig<=1 then endwhile
                    delta = delta / refineratio; % deltaを少し大きくする.. 必ず whileは終わるはず
                    evalinv = 1./ (delta + lambda );
                    if(sum(isinf(evalinv)) || sum(isnan(evalinv))) % div by zero
                        delta = 1e-8;
                        evalinv = 1./ (delta + lambda );
                    end
                    M = S2S1t * U*diag(evalinv)*U'; %
                    maxeig = max(abs(eig( [zeros(dim1-dim2, dim2) eye(dim1-dim2); M] ))); %*
                end
            end
            %% final result
            gamma =  sum( lambda ./ (delta + lambda) ); % effective num
            alpha = (dim2*gamma)/norm(M, 'fro')^2;
            beta = (dim2*(len-gamma))/ norm(S2 - M*S1, 'fro')^2; % Note: No16
            if(beta > 100)
                beta = 100;
            end
            %
            val1 = alpha;
            val2 = beta;
            val3 = gamma;
            %     ylabelstr = {'\alpha', '\beta', '\gamma', '\delta'};
            %     figure(10)
            %     for i=1:4
            %       subplot(4,1,i); plot(paramlog(:,i)); ylabel(ylabelstr{i}); xlabel('iteration');
            %     end
            %    fprintf('alpha = %.3e, beta = %.3e, gamma = %.3f, delta = %.3e\n', alpha, beta, gamma, delta);
        end
    elseif (bayesian==-1)
        %% testing 直接的にdeltaをつかう
        [U evYY] = eig(S1S1t);
        lambda = diag(evYY);
        delta = 1e6;
        for it=1:10000
            prev_delta = delta;
            delta = delta * 0.8; % 次第に小さく
            M = S2S1t * U*diag( 1./ (delta + lambda ) )*U'; %
            if( norm(M, 'fro')^2/dim2 > upper_bound) % ridge regularization
                M = S2S1t * U*diag( 1./ (prev_delta + lambda ) )*U'; %
                break;
            end
        end
        fprintf('it: %d, delta: %.3e, dim2: %d, norm(M)^2/dim2: %.3f\n', it, prev_delta, dim2, norm(M, 'fro')^2/dim2);

    else % use Gershgorin's theorem (+ refinement )
        if (maxeig > 1)
            %% Ridge regression
            %   if( sum(sum(isinf(S1))) || sum(sum(isnan(S1))) )
            %     S1
            %   end
            [U sv V] = svd(S1, 'econ');
            rdim = size(U, 2);
            sv = diag(sv);
            Z = S2 * V;
            w = cat(3);
            for k=1:rdim
                w = cat(3, w, Z(:,k)*U(:,k)');
            end

            p = zeros(dim2,rdim);
            for r = 1:dim2
                for k = 1:rdim
                    p(r,k) = norm( w(r,:,k), 1 );
                end
            end
            % select single row based on  max_r p(r,1)
            [max_p1val, max_p1row] = max(p(:,1));
            %% calc delta
            if(calceachrow==1)
                M = zeros(dim2,dim1);
                d2eachrow = zeros(dim2,1);
                for r=1:dim2
                    Ccoef = p(r, :);
                    Dcoef = squeeze( w(r, :, :) ); % slice of w at row r
                                                   % initialized by the smallest sv
                    d2 = calc_deltasqr(sv(rdim)^2, sv, Ccoef, Dcoef, maxit, upper_bound, step1thres, step2ratio);
                    denom = S1S1t + eye(dim1) * d2;
                    if ( sum(sum(isinf(denom))) || sum(sum(isnan(denom)) ) )
                        d2 = 1e-8;
                        denom = S1S1t + eye(dim1) * d2;
                    end
                    M(r,:) = S2S1t(r,:) * inv(denom);     %% calculate soft pinv , TODO : use eigvecs
                    d2eachrow(r) = d2;
                end
                %d2eachrow'
            else
                Ccoef = p(max_p1row, :);
                Dcoef = squeeze( w(max_p1row, :, :) ); % slice of w at row r
                                                       % initialized by the smallest sv
                [d2 d2list] = calc_deltasqr(sv(rdim)^2, sv, Ccoef, Dcoef, maxit, upper_bound, step1thres, step2ratio);
                %d2
                evalinv = 1./ (d2 + sv.^2 );
                if(sum(isinf(evalinv)) || sum(isnan(evalinv))) % div by zero
                    d2 = 1e-8;
                    evalinv = 1./ (d2 + sv.^2 );
                end
                M = S2S1t * U*diag(evalinv)*U'; %
                                                %      denom = S1S1t + eye(dim1) * d2;
                                                %       if ( sum(sum(isinf(denom))) || sum(sum(isnan(denom)) ) )
                                                %         d2 = 1e-8;
                                                %         denom = S1S1t + eye(dim1) * d2;
                                                %       end
                                                %       M = S2S1t * inv(denom);     %% calculate soft pinv
                %% refinement ( check maxeig ) できるだけ1に近くする
                if(calceachrow<0)
                    if ~checkLSeig  % if not checked yet
                        Mtmp = S2 * pinv(S1);
                        maxeig= max(abs(eig( [zeros(dim1-dim2, dim2) eye(dim1-dim2); Mtmp] ))); %*
                        if (maxeig < upper_bound)
                            M = Mtmp;
                            return;
                        end
                    end
                    maxeig = max(abs(eig( [zeros(dim1-dim2, dim2) eye(dim1-dim2); M] ))); %*
                    detectinfloop_flag = 0;
                    while (maxeig <= upper_bound ) % to satisfy maxeig<=1
                        d2 = d2 * refineratio;
                        d2list = [d2list d2];
                        evalinv = 1./ (d2 + sv.^2 );
                        if(sum(isinf(evalinv)) || sum(isnan(evalinv))) % div by zero
                            d2 = 1e-8;
                            evalinv = 1./ (d2 + sv.^2 );
                            if(detectinfloop_flag == 1)
                                break;
                            else
                                detectinfloop_flag = 1;
                            end

                        end
                        M = S2S1t * U*diag(evalinv)*U'; %
                                                        %           denom = S1S1t + eye(dim1) * d2;
                                                        %           if ( sum(sum(isinf(denom))) || sum(sum(isnan(denom)) ) )
                                                        %             d2 = 1e-8;
                                                        %             denom = S1S1t + eye(dim1) * d2;
                                                        %             if(detectinfloop_flag == 1)
                                                        %               break;
                                                        %             else
                                                        %               detectinfloop_flag = 1;
                                                        %             end
                                                        %           end
                                                        %          M = S2S1t * inv(denom);     %% calculate soft pinv
                        maxeig = max(abs(eig( [zeros(dim1-dim2, dim2) eye(dim1-dim2); M] ))); %*
                    end
                    d2 = d2 / refineratio;
                    evalinv = 1./ (d2 + sv.^2 );
                    if(sum(isinf(evalinv)) || sum(isnan(evalinv))) % div by zero
                        d2 = 1e-8;
                        evalinv = 1./ (d2 + sv.^2 );
                    end
                    M = S2S1t * U*diag(evalinv)*U'; %
                                                    %         denom = S1S1t + eye(dim1) * d2;
                                                    %         if ( sum(sum(isinf(denom))) || sum(sum(isnan(denom)) ) )
                                                    %           d2 = 1e-8;
                                                    %           denom = S1S1t + eye(dim1) * d2;
                                                    %         end

                    d2list = [d2list d2];
                    %        M = S2S1t * inv(denom);     %% calculate soft pinv
                    %maxeig = max(abs(eig( [M; eye(dim1-dim2) zeros(dim1-dim2, dim2)] )))
                end
            end

            % plot delta^2 history
            %figure(99); semilogy (d2list);
        end
    end


    %%
    if centerize
        bias = mean2 - M * mean1;
    else
        bias = zeros(size(S1,1), 1);
    end

end


function [d2 d2list] = calc_deltasqr(d2init, sv, Ccoef, Dcoef, maxit, upper_bound, step1thres, step2ratio)
%% CALC_DELTASQR
%
    skipstep2 = 0;
    svsqr = sv.^2;
    d2list = [];
    %% step 1    % find d2 (delta^2) by using Newtonw's Method
    d2 = d2init;
    if(d2 < 1e-8)
        d2 = 1e-8;
    end
    tryagain = 0;
    d2list = [d2list; d2];
    for it=1:maxit
        smoothed_invsv = sv ./ (svsqr + d2);
        B1 = Ccoef * smoothed_invsv; % (3.16)  rowvec * colvec
        B1deriv = - Ccoef * (smoothed_invsv ./ (svsqr + d2));
        d2prev = d2;
        d2 = d2 - (B1 - upper_bound) / B1deriv;
        d2list = [d2list d2];
        % check updated d2 < d2prev
        if ( d2 - d2prev < 0)
            d2 = 1e-8;
            if ( tryagain )
                skipstep2 = 1;
                break;
            else
                tryagain = 1; % try again by using newly initialized d2
            end
        else
            if (d2 / d2prev < step1thres)
                break;
            end
        end
    end

    %% step 2
    if( ~skipstep2 )
        for it=1:maxit
            B2 = norm( Dcoef * (sv ./ (svsqr + d2)), 1);
            if( B2 >= upper_bound)  % B2 > upper_bound
                d2 = d2 / step2ratio;
                d2list = [d2list d2];
                break;
            end
            d2 = d2 * step2ratio; % d2 -> decrease
            d2list = [d2list d2];
        end
    end

end
