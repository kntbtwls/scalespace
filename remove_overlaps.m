function Iresult = remove_overlaps(Iset)
%% REMOVE_OVERLAPS
% this is the special case of remove_overlaps_Itotalset

if isempty(Iset)
  Iresult = [];
  return;
end

Iset = [Iset ones( size(Iset, 1), 1) ];
Iresult = remove_overlaps_from_Itotalset( Iset );
Iresult = Iresult(:, 1:2);

end

%endmax = max(Iset(:, 2));
%flag = zeros(1,endmax);

% % set OR
% for k=1:size(Iset, 1)
%   flag(Iset(k,1):Iset(k,2)) = 1;
% end
%
% % extract beg and end points
% Iresult = [];
% I = zeros(1,2);
% open = 0;
% for t=1:endmax
%   if (flag(t) && ~open)
%     I(1) = t;
%     open = 1;
%     continue;
%   end
%   if (~flag(t) && open)
%     I(2) = t-1;
%     open = 0;
%     Iresult = [Iresult; I];
%     continue;
%   end
% end
% if (open)
%   I(2) = endmax;
%   Iresult = [Iresult; I];
% end
