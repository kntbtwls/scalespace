function [ clrefresult, refinedgenseq, Itotalset_set ] = ...
        display_refinedgenseq( Xs, clresult, order, dispseqid, algo_for_refine, refine_upper_bound, Nlist, lmaxptr)
    %% display result
    % TODO: move Itotalset_set to the return of hclust
    % Nlist: list of N to be displayed

    %refine_upper_bound = 1.0;
    %algo_for_refine = 14; % for final refinement
    centerize = 1;
    skip_refinement = 0;

    Nmax = size(clresult,2);
    DispNnum = length(Nlist); % the numbers of N to be displayed
                              %nsamples = size(Xs, 2)

    maxidx = find_maxidx_of_Iset( clresult );
    T = maxidx(dispseqid);

    Errnormlist = zeros(Nmax, 1);
    refinedErrnormlist = zeros(Nmax, 1);

    figure('Name', 'hclust', 'NumberTitle', 'off');
    nfigcols = 2; % 4 (2010.3.29)
                  % find range of the original data
    ymax = max(max(Xs{dispseqid}(:,1:T)));
    ymax = ymax * 1.2;
    ymin = min(min(Xs{dispseqid}(:,1:T)));
    ymin = ymin * 1.2;
    % plot orig data
    for frow=1:nfigcols
        subplot(DispNnum+1,nfigcols,frow); plot(Xs{dispseqid}(:, 1:T)');  ylim([ymin,ymax]); title('Original Data'); ylabel('State x')
        xlim([0 length(Xs{dispseqid}(:, 1:T)')])
        ylm = get(gca, 'YLim');
        for l=1:length(lmaxptr)
            line([lmaxptr(l),lmaxptr(l)], ylm, 'Color', 'k');
        end
    end
    %axis ([ 0 totallen 0.5 2.5 ]);
    outputseq = cell(1,Nmax); % result
    clrefresult = cell(1,Nmax); % result
    Itotalset_set = cell(1,Nmax);

    dispIdx = 1;
    for N = Nlist
        fprintf('output result and gen seq.  (N = %d)\n', N);
        %## identify refined LDSs
        clrefresult{N} = cell(1,N); % refined LDSs
        for i=1:N % for each LDS
            cl = clresult{N}{i};
            [clrefresult{N}{i}.A clrefresult{N}{i}.b Qtmp clrefresult{N}{i}.xinit] = identify_system(Xs, cl.Iset, algo_for_refine, centerize, skip_refinement, refine_upper_bound, order);
            n = size(clrefresult{N}{i}.A, 2);
            %    fprintf('N=%d... i: %d, maxabseig_A: %.4f\n', N, i, max(abs(eig( [zeros(n*order-n, n) eye(n*order-n); clrefresult{N}{i}.A] ))) );
        end
        %## generation
        Itotalset = [];
        for i=1:size(clresult{N},2) % for each LDS
            Isettmp = clresult{N}{i}.Iset{dispseqid}; % extract LDS_i, sampleID=dispseqid
            Itotalset = [Itotalset; Isettmp ones(size(Isettmp, 1), 1)*i];
        end
        Itotalset = sortrows(Itotalset, 1);
        Itotalset_set{N} = Itotalset;
        genseq = generate_total_sequence(clresult{N}, Itotalset, 0);
        refinedgenseq = generate_total_sequence(clrefresult{N}, Itotalset, 0);
        refinedgenseq_continuous = generate_total_sequence(clrefresult{N}, Itotalset, 1);
        ldsseq = zeros(T, 1); % interval sequence
        for k=1:size(Itotalset, 1)
            ldsseq( Itotalset(k,1):Itotalset(k,2) ) = Itotalset(k,3); % overlap each preceding interval
        end

        %## display
        % 2010.3.30
        subplot(DispNnum+1,nfigcols, nfigcols*(DispNnum-dispIdx+1)+1); ...
            plot(ldsseq); ylim([0.8, N+0.2]);
        xlim([0 length(Xs{dispseqid}(:, 1:T)')])
        title('Estiamted Mode Sequence'); ylabel('Mode ID');

        %subplot(Nmax+1,nfigcols, nfigcols*(Nmax-N+1)+1); plot(ldsseq); ylim([0.8, N+0.2]); title('Estiamted Mode Sequence'); ylabel('Mode ID');
        subplot(DispNnum+1,nfigcols, nfigcols*(DispNnum-dispIdx+1)+2); ...
            plot(genseq');
        xlim([0 length(Xs{dispseqid}(:, 1:T)')])
        ylim([ymin,ymax]); title('Generated from Estimated Parameters and Modes'); ylabel('State x');
        %subplot(Nmax+1,nfigcols, nfigcols*N+1); plot(ldsseq); ylim([0.8, N+0.2]); title('Mode Sequence'); ylabel('Mode ID');
        %subplot(Nmax+1,nfigcols, nfigcols*N+2); plot(genseq'); ylim([-1.1,1.1]); title('Generated Data'); ylabel('State x');
        %subplot(Nmax+1,nfigcols, nfigcols*N+3); plot(refinedgenseq');  ylim([-1.1,1.1]);title('Eigenvalue Constraint');
        %subplot(Nmax+1,nfigcols, nfigcols*N+4); plot(refinedgenseq_continuous');  ylim([-1.1,1.1]);title('EC (init by prev states)');

        %  fprintf('Errnorm: %.4f\n', norm(X - genseq, 'fro') );
        %  subplot(Nmax,2, 2*(Nmax-N+2)); plot(overlaps)

        Errnormlist(N) = norm(Xs{dispseqid}(:, 1:T) - genseq, 'fro');
        refinedErrnormlist(N) = norm(Xs{dispseqid}(:, 1:T) - refinedgenseq, 'fro');

        outputseq{N} = refinedgenseq;
        dispIdx = dispIdx + 1;
    end
end
