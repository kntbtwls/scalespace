%%% RUN THIS FILE
clear variables
close all

addpath('../ConstrainedSID', '../ClusteringLDS');

% 特徴量データ準備
load ('./data/points_each.mat')

signal_A = points_each(:,1,49);
signal_B = points_each(:,1,55);
signal = signal_B - signal_A;

smoothing_windowsize = 5; % 元信号に対する平滑化

signal = smooth(signal, smoothing_windowsize, 'moving'); % 平滑化いれることにする
signal=signal';


%% 変曲点検出
normseq = var_norm(signal); % フレーム間差分ベクトルの系列
sigma = 1:100;
C0 = scalespace(normseq, sigma);
C = diff(C0,2,2); % 二次差分 (第2引数が差分の回数)
vidlen = length(normseq);
lmax = zeros(size(C,1), size(C,2));

for i = 1:size(C,1)
    lmax(i,:) = zerocross(C(i,:));
end

C_old = C;
lmax_old = lmax;
% ゼロクロスが一度も出てこないスケールを削除
C=C(any(lmax'),:);
lmax=lmax(any(lmax'),:);

lmax_old2 = lmax;

% 系列の始めの方と終わりの方の変曲点は使わない
EDGE = 9;
lmax(:,[1:1+EDGE,end-EDGE:end]) = 0;

%% いくつかplotして確認（川嶋）

target_s = 20; % どのレベルの変曲点を用いるか

% ---ピーク検出---
% 元の信号を標準化し，振幅の情報を使えるようにする
s_signal = smooth(signal, 8, 'moving'); % さらに平滑化しておく
mean_signal = mean(s_signal);
std_signal = std(s_signal);
nsignal = bsxfun(@minus, s_signal, mean_signal); % それぞれから平均を引く
nsignal = nsignal/std_signal;

pkthres = 0.002; %## param ## 平らなピークを除外（ピークとその両隣との間には，最小振幅差としてpkthresが必要とする）
[allpospk_vals, allpospk_locs] = findpeaks(nsignal, 'Threshold', pkthres); %locはlocationの略
[allnegpk_vals, allnegpk_locs] = findpeaks(-nsignal, 'Threshold', pkthres);
allinflpts = find(lmax(target_s,:)>0)'; % 変曲点のリスト
fprintf('# of inflection points: %d, # of pos peak: %d, # of neg peak: %d\n',...
    length(allinflpts), length(allpospk_locs), length(allnegpk_locs));

%trange = 1:1800;
%trange = 1100:1600; % 表示する範囲
trange= 1:length(signal)-3; % 二次差分だから -2 でいいはずだが念のため
sigmalist = [5, 10, 20]; % 参考までにプロットするスケールレベル (*)
baserow = 2; % for loop の前にプロットする個数
Nsubfig = baserow + 2*length(sigmalist);

figure('Name', 'check original signals', 'Position', [50,50,1800,950]);
subplot(Nsubfig,1,1), plot(trange, signal(trange), 'k'), ylabel('signal');
pospklocs = allpospk_locs(allpospk_locs >= trange(1) & allpospk_locs <= trange(end)); % プロット範囲内だけ取り出す
negpklocs = allnegpk_locs(allnegpk_locs >= trange(1) & allnegpk_locs <= trange(end)); % プロット範囲内だけ取り出す
inflpts = allinflpts(allinflpts >= trange(1) & allinflpts <= trange(end)); % プロット範囲内だけ取り出す
hold on
plot(pospklocs, signal(pospklocs), '^', 'Color', [0.7, 0.3, 0]), plot(negpklocs, signal(negpklocs), 'v', 'Color', [0, 0.3, 0.7])
plot(inflpts, signal(inflpts), 'x', 'Color', [0, 0.6, 0.2]);
hold off
dsignal = diff(signal, 1,2); % 単に差分を取ってみる (絶対値やノルムはとらない)
subplot(Nsubfig,1,2), plot(trange, dsignal(trange)), ylabel('diff signal');
line([trange(1),trange(end)], [0,0], 'LineStyle', ':');
k=1;

% 参考までに sigmalistで指定したスケールの変曲点も下に並べてプロット
for s=sigmalist
    subplot(Nsubfig,1,baserow+(k-1)*2+1), plot(trange, C0(s,trange),'LineWidth',2); ylabel(sprintf('C0(%d)', s)); a1 = gca;
    subplot(Nsubfig,1,baserow+(k-1)*2+2), plot(trange, C(s,trange),'LineWidth',2); ylabel(sprintf('C(%d)', s)); a2 = gca;
    ylm = get(gca, 'YLim');
    tmpinflpts = find(lmax(s,:)>0)'; % 変曲点のリスト
    inflpts = tmpinflpts(tmpinflpts >= trange(1) & tmpinflpts <= trange(end));
    for l=1:length(inflpts)
        line([inflpts(l),inflpts(l)], ylm, 'Color', 'k');
    end
    axes(a1)
    ylm = get(gca, 'YLim');
    for l=1:length(inflpts)
        line([inflpts(l),inflpts(l)], ylm, 'Color', 'k');
    end
    k = k+1;
end

%% 区間系列を生成 (川嶋)

divpts = sort([allinflpts; allpospk_locs; allnegpk_locs]); % s_targetレベルの変曲点, 正のピーク, 負のピークすべて候補とする
% divpts = allinflpts; % s_targetレベルの変曲点のみを候補とする
fprintf('Number of inflection points: %d\n', length(divpts));
Iset = [1, divpts'; divpts'-1, size(lmax,2)]'; % 区間系列
save('signal.mat', 'signal');

divCost = 0.02;
max_interval_cat = 40;
%max_interval_cat = length(divpts);
[newIset, totalerror, intervalerrors] = viterbi_concatenate_intervals(Iset, signal, divCost, max_interval_cat); % 長さに関するViterbi

% 結果表示
figure('Name', 'segmentation');
subplot(211)
plot_X_with_divpoints(signal, Iset);
fprintf('Number of intervals (concatenated by Viterbi): %d\n', size(newIset,1));
subplot(212)
plot_X_with_divpoints(signal, newIset);


%% 各区間の始点や変化特徴を散布図でプロット (川嶋)
newK = size(newIset,1);
ldsprm = zeros(newK, 7);
for k=1:newK
    [sqerr, A, b] = simpleAR(signal, newIset(k,1), newIset(k,2));
    ldsprm(k, :) = [k, sqerr, A, b, signal(newIset(k,1)), signal(newIset(k,2)), newIset(k,2)-newIset(k,1)+1];
    % 二乗誤差, A, b, xinit, xfin, len
end

figure('Name', 'LDSfeatures');
%scatter3(ldsprms(:,5), ldsprms(:,6), ldsprms(:,3)), xlabel('xinit'), ylabel('xfin'), zlabel('A')
scatter(ldsprm(:,5), ldsprm(:,6)-ldsprm(:,5)), xlabel('xinit'), ylabel('xfin-xinit')
for k=1:newK
    text(ldsprm(:,5), ldsprm(:,6)-ldsprm(:,5), ldsprm(:,1))
end



return

%% 変曲点のトレース
startlevel=20;
endlevel=1;
lmaxlist=find(lmax(startlevel,:)>0); % 一番上のscale で lmax==1 と
                                     % なるインデックス(時刻)を見つける
lmaxtrace=ones(size(lmaxlist)); % どのスケールまでトレースできたか
                                % の記録用
lmaxptr=zeros(size(lmaxlist));  % 最終出力

for l=1:length(lmaxlist)    % 最大scaleでlmax==1であった各点に関して
    lmaxptr(l)=lmaxlist(l); % 最大スケールからスタート
    trackflag=true;

    for s=startlevel-1:-1:endlevel % 最大スケールから順に下へ
        clmaxlist=find(lmax(s,:)>0); % scale=s における変曲点の時刻
        nlmaxlist=find(lmax(s+1,:)>0); % scale=s+1における変曲点の時刻
        [m,idx]=min(abs(clmaxlist-lmaxptr(l))); % 時間軸上でlmaxptrと近い点をclmaxlistから見つける
        [m2,idx2]=min(abs(nlmaxlist-clmaxlist(idx))); % 時間軸上でclmaxlist(idx)と近い点をnlmaxlistからみつける

        if(lmaxptr(l)==nlmaxlist(idx2)&&trackflag) % lmaxptr->clmaxlist->nlmaxlistと近いものを辿って一致した場合
            lmaxtrace(l)=s;
            lmaxptr(l)=clmaxlist(idx); % lmaxptr は 現在のスケールでのトレース結果とする
        else
            trackflag=false; % トレース終了
        end

    end

end

%% 結果表示
if(1)
    figure('Name', 'Result of lmaxseq', 'Position', [50, 200, 1250, 600])
    subplot(3,1,1)
    plot(signal)
    xlim([0 length(signal)])
    ylm = get(gca, 'YLim');
    for l=1:length(lmaxptr)
        line([lmaxptr(l),lmaxptr(l)], ylm, 'Color', 'k');
    end
    subplot(3,1,2)
    plot([normseq',C0(startlevel,:)',C0(endlevel,:)']), title('Input')
    xlim([0 length(signal)])
    ylm = get(gca, 'YLim');
    for l=1:length(lmaxptr)
        line([lmaxptr(l),lmaxptr(l)], ylm, 'Color', 'k');
    end
    subplot(3,1,3)
    imagesc(lmax), title('Inflection points') % maxima of acceleration
    xlim([0 length(signal)])
end

%% クラスタリング
% Xs{1} = signal';
% order = 1;
% Nmax = 64;
% manualQdiag_hclust = 0.01;
% algo_for_clustering = 15;
% upper_bound = 1.0;

% clresult = clustering(Xs, order, Nmax, manualQdiag_hclust, ...
%                       algo_for_clustering, upper_bound, lmaxptr);

% dispseqid = 1;
% algo_for_reidentify = 15;
% refine_upper_bound = 1.0;
% Nlist = 8:8:32;
% [ clrefresult, refgenseq, Itotalset_set ] = ...
%     display_refinedgenseq(Xs, clresult, order,...
%                           dispseqid, algo_for_reidentify, ...
%                           refine_upper_bound, Nlist, lmaxptr);

lmaxptrn = segmentation([1 lmaxptr length(signal)], signal);

for i = 1:length(lmaxptrn)-1
    seg{1} = signal(1, lmaxptrn(i):lmaxptrn(i+1));
    [~, ~, genseq] = easyAR(seg);
    if(i==1)
        genseqs= genseq;
    else
        genseqs = horzcat(genseqs(1, 1:length(genseqs)-1), genseq);
    end
end

figure('Name', 'crusterized', 'Position', [50, 200, 1250, 600])
subplot(211)
plot(signal)
xlim([0 length(signal)])
ylm = get(gca, 'YLim');
for l=1:length(lmaxptrn)
    line([lmaxptrn(l),lmaxptrn(l)], ylm, 'Color', 'k');
end
subplot(212)
plot(genseqs)
xlim([0 length(signal)])
ylm = get(gca, 'YLim');
for l=1:length(lmaxptrn)
    line([lmaxptrn(l),lmaxptrn(l)], ylm, 'Color', 'k');
end
