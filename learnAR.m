function [Ahat, Qhat, Err, bhat, alpha, beta, gamma, Acovinv] = learnAR(S1, S2, algo, centerize, additionalflag, additionalval)
%%
% 'centerize' should be 0 for LB1, LB2??
%
alpha = 0;
beta  = 100; % emptyや変化なしなどでbetaが決まらなかったとき用 (BFDistanceで使う)
gamma = 0;
Acovinv = 0;
calc_Acov = 0; % turn on if Bayesian

if isempty(additionalval)
    additionalval = 1.0;
end
if isempty(additionalflag)
    additionalflag = 0;
end

%%
if isempty(S2)
    if(isempty(S1))
        Ahat = 0;
        Qhat = 0;
        Err = 0;
        bhat = 0;
        return;
    end
    disp('isempty(S2)');
    S2 = S1(:, 2:end);
    S1 = S1(:, 1:end-1);
end

[dim1, len] = size(S1);
dim2 = size(S2,1);

mean1 = zeros(dim1,1);
mean2 = zeros(dim2,1);
S1orig = S1;
S2orig = S2;

if ( centerize )
    mean1 = mean(S1, 2);
    mean2 = mean(S2, 2);
    S1 = S1 - mean1*ones(1, len);
    S2 = S2 - mean2*ones(1, len);
end

%%
% if(len >= dim*3)
%   algo = 11;
% end

if ( norm(S1, 'fro') < 1e-16 )
    %fprintf('static seq: %.4f\n', norm(S1, 'fro') );
    %   Ahat = (1 - 1/len) * eye(dim);
    %   bhat = S2(:,end)/len;
    Ahat = zeros( dim2, dim1 );
    bhat = mean2;
    Err = zeros(size(S1));
    Qhat = 0;
    return;
end

switch algo
  case 1    % Least Squares
            %        disp('Learning Dynamics Matrix using Least Squares');
    Ahat = S2orig*pinv(S1);

  case 2    % Constraint Generation
            %        disp('Learning Dynamics Matrix using Constraint Generation');
    Ahat = learnCGModel(S1,S2,0);

  case 3    % Lacy Bernstein 1
            %        disp('Learning Dynamics Matrix using Lacy Bernstein 1');
    Ahat = learnLB1Model(S1,S2);

  case 4    % Lacy Bernstein 1 simulated using CG
            %        disp('Learning Dynamics Matrix using Simulated Lacy Bernstein 1');
    Ahat = learnCGModel(S1,S2,1);

  case 5    % Lacy Bernstein 2
            %        disp('Learning Dynamics Matrix using Lacy Bernstein 2');
    Ahat = learnLB2Model(S1,S2);

  case 6    % Sparse
    Ahat = learnSparseModel(S1,S2, additionalval, 0, 0, additionalflag, 0);

  case 7    % Sparse check LS solution and skip lars if eig<=1
    Ahat = learnSparseModel(S1,S2, additionalval, 1, 0, additionalflag, 0);

  case 8    % LARS (normalized)
    Ahat = learnLARModel(S1,S2, additionalval, 1, 0);

  case 9    % LARS (normalized) check LS solution and skip lars if eig<=1
    Ahat = learnLARModel(S1,S2, additionalval, 1, 1);

  case 10    % LARS (without normalized)
    Ahat = learnLARModel(S1,S2, additionalval, 0, 0);

  case 11    % Gershgorin, using Ridge regression (calc delta for each row)
    Ahat = learnSoftPinvModel(S1,S2, additionalval, 0, 0, 1, 0);

  case 12    % Gershgorin, using Ridge regression for all rows (calc single delta for a matrix)
    Ahat = learnSoftPinvModel(S1,S2, additionalval, 0, 0, 0, 0);

  case 13    % Gershgorin + refine, using Ridge regression for all rows (calc single delta for a matrix)
    Ahat = learnSoftPinvModel(S1,S2, additionalval, 0, 0, -1, 0);

  case 14    % Bayesian estimation (ridge regression) for each rows
    usebayesianflag = 1;
    [Ahat, dummy, alpha, beta, gamma] = learnSoftPinvModel(S1,S2, additionalval, 0, 0, 1, usebayesianflag);
    calc_Acov = 1;

  case 15    % Bayesian estimation (ridge regression) for all rows
    usebayesianflag = 1;
    [Ahat, dummy, alpha, beta, gamma] = learnSoftPinvModel(S1,S2, additionalval, 0, 0, 0, usebayesianflag);
    calc_Acov = 1;

  case 16    % testing
    usebayesianflag = 1;
    [Ahat, dummy, alpha, beta, gamma] = learnSoftPinvModel(S1,S2, additionalval, 0, 0, -1, usebayesianflag);
    calc_Acov = 1;

  case 20    % testing
    usebayesianflag = -1;
    [Ahat, dummy, alpha, beta, gamma] = learnSoftPinvModel(S1,S2, additionalval, 0, 0, 0, usebayesianflag);

  case 30    % testing
    usebayesianflag = -1;
    [Ahat, dummy, alpha, beta, gamma] = learnKernelModel(S1,S2, additionalval, 0, 0, 0, usebayesianflag);

  otherwise
    disp('invalid algo parameter');
end

if( calc_Acov==1 )
    Acovinv = diag([0; alpha*ones(dim1,1)]) + beta*[ones(1,len); S1orig]*[ones(len,1) S1orig'];
else
    Acovinv = 0;
end


% if ( centerize )
%   bhat = mean2 - Ahat * mean1;
% else
%   bhat = zeros(size(S1,1), 1);
% end

bhat = mean2 - Ahat * mean1;
Err = S2-Ahat*S1;
% if S1, S2 are centerized, the above Err is identical to the result of
%  Err = S2orig - Ahat*S1orig - bhat*ones(1,len);

% Estimate Evolution and Observation Noise Covariance Matrices

Qhat = cov(Err');
