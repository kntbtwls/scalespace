function [ signal_smoothed ] = scalespace( signal, sigma )
% 時系列データを平滑化
% signalは行方向の時系列データ
% sigmaは利用するσの値の集合

% gaussianを畳み込む
palam = 10;   % 窓の幅。sigmaの比
for s = 1:size(sigma, 2)
    winsize = palam * sigma(1,s) + 1;
    window = fspecial('gaussian', [winsize,1], sigma(1,s));
    window = window / sum(window);

    signal_smoothed(s,:) = convn(signal, window', 'same');
end

end
