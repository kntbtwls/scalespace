function [ out ] = extract( signal, subset, num )
% signalはnum x 3列
% subset番目のxy座標をそれぞれ取ってくる
out = [signal(:, subset), signal(:, subset+num)];
end
