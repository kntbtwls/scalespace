clear all;
close all;

num = 66;

load('./data/points_each.mat');
load('./data/points_xyz.mat');
load('./data/points_198.mat');

jaw = 1:17;
eyebrow_r = 18:22;
eyebrow_l = 23:27;
nose_line = 28:31;
nose_under = 32:36;
eye_r = 37:42;
eye_l = 43:48;
mouth = 49:66;

Fs = 23.928380;

points_parts = {extract(points_198, jaw, num),
                extract(points_198, eyebrow_r, num),
                extract(points_198, eyebrow_l, num),
                extract(points_198, nose_line, num),
                extract(points_198, nose_under, num),
                extract(points_198, eye_r, num),
                extract(points_198, eye_l, num),
                extract(points_198, mouth, num)};

variation_norm_all_smoothed_s2 = scalespace(var_norm(points_198(:, 1:2*num)), 2);
variation_norm_mouth = var_norm(points_parts{8,1});
variation_norm_mouth_smoothed_s2 = scalespace(variation_norm_mouth, 2);
variation_norm_eye_r = var_norm(points_parts{6,1});
variation_norm_eye_r_smoothed_s2 = scalespace(variation_norm_eye_r, 2);

figure(1);
h1 = subplot(3,1,1);
t = (0:1./Fs:6461*Fs)';
t = t(1:size(variation_norm_all_smoothed_s2, 2));
plot(t, variation_norm_all_smoothed_s2);
xlabel('Time (s)');
ylabel('all(var/norm)');
xlim([0, 270]);
h2 = subplot(3,1,2);
t = (0:1./Fs:6461*Fs)';
t = t(1:size(variation_norm_eye_r_smoothed_s2, 2));
plot(t, variation_norm_eye_r_smoothed_s2);
xlabel('Time (s)');
ylabel('R eye(var/norm)');
xlim([0, 270]);
h3 = subplot(3,1,3);
t = (0:1./Fs:6461*Fs)';
t = t(1:size(variation_norm_mouth_smoothed_s2, 2));
plot(t, variation_norm_mouth_smoothed_s2);
xlabel('Time (s)');
ylabel('mouth(var/norm)');
xlim([0, 270]);

Wsize = 15;
scrollsubplot(Wsize, t, h1, h2, h3);

figure(2);
subplot(3,1,1);
t = (0:1./Fs:6461*Fs)';
t = t(1:size(variation_norm_all_smoothed_s2, 2));
plot(t, variation_norm_all_smoothed_s2);
xlabel('Time (s)');
ylabel('all(var/norm)')
xlim([0, 270]);
subplot(3,1,2);
plot(t, variation_norm_eye_r_smoothed_s2);
xlabel('Time (s)');
ylabel('R eye(var/norm)');
xlim([0, 270]);
subplot(3,1,3);
plot(t, variation_norm_mouth_smoothed_s2);
xlabel('Time (s)');
ylabel('mouth(var/norm)');
xlim([0, 270]);

variation_norm_mouth_smoothed_s4 = scalespace(variation_norm_mouth, 4);
variation_norm_mouth_smoothed_s8 = scalespace(variation_norm_mouth, 8);
variation_norm_mouth_smoothed_s16 = scalespace(variation_norm_mouth, 16);
variation_norm_mouth_smoothed_s32 = scalespace(variation_norm_mouth, 32);
figure('Name', 'Norm of variation(scalespaced)');
subplot(2,1,1)
t = (0:1./Fs:6461*Fs)';
t = t(1:size(variation_norm_mouth_smoothed_s2, 2));
plot(t, variation_norm_mouth_smoothed_s2);
xlabel('Time (s)');
ylabel('\sigma=2');
xlim([0, 270]);
% figure('Name', 'Norm of variation(scalespaced)');
% t = (0:1./Fs:6461*Fs)';
% t = t(1:size(variation_norm_mouth_smoothed_s2, 2));
% plot(t, variation_norm_mouth_smoothed_s4);
% xlabel('Time (s)');
% ylabel('\sigma=4');
% xlim([0, 270]);
% figure('Name', 'Norm of variation(scalespaced)');
% t = (0:1./Fs:6461*Fs)';
% t = t(1:size(variation_norm_mouth_smoothed_s2, 2));
% plot(t, variation_norm_mouth_smoothed_s8);
% xlabel('Time (s)');
% ylabel('\sigma=8');
% xlim([0, 270]);
% figure('Name', 'Norm of variation(scalespaced)');
% t = (0:1./Fs:6461*Fs)';
% t = t(1:size(variation_norm_mouth_smoothed_s2, 2));
% plot(t, variation_norm_mouth_smoothed_s16);
% xlabel('Time (s)');
% ylabel('\sigma=16');
% xlim([0, 270]);
% figure('Name', 'Norm of variation(scalespaced)');
subplot(2,1,2)
t = (0:1./Fs:6461*Fs)';
t = t(1:size(variation_norm_mouth_smoothed_s2, 2));
plot(t, variation_norm_mouth_smoothed_s32);
xlabel('Time (s)');
ylabel('\sigma=32');
xlim([0, 270]);

variation_norm_mouth_smoothed_s2_2nd_deriv = diff(diff(variation_norm_mouth_smoothed_s2));
variation_norm_mouth_smoothed_s4_2nd_deriv = diff(diff(variation_norm_mouth_smoothed_s4));
variation_norm_mouth_smoothed_s8_2nd_deriv = diff(diff(variation_norm_mouth_smoothed_s8));
variation_norm_mouth_smoothed_s16_2nd_deriv = diff(diff(variation_norm_mouth_smoothed_s16));
variation_norm_mouth_smoothed_s32_2nd_deriv = diff(diff(variation_norm_mouth_smoothed_s32));

variation_norm_mouth_smoothed_2nd_deriv_zeros(1,:) = zerocross(variation_norm_mouth_smoothed_s2_2nd_deriv);
variation_norm_mouth_smoothed_2nd_deriv_zeros(2,:) = zerocross(variation_norm_mouth_smoothed_s4_2nd_deriv) * 2;
variation_norm_mouth_smoothed_2nd_deriv_zeros(3,:) = zerocross(variation_norm_mouth_smoothed_s8_2nd_deriv) * 3;
variation_norm_mouth_smoothed_2nd_deriv_zeros(4,:) = zerocross(variation_norm_mouth_smoothed_s16_2nd_deriv) * 4;
variation_norm_mouth_smoothed_2nd_deriv_zeros(5,:) = zerocross(variation_norm_mouth_smoothed_s32_2nd_deriv) * 5;

figure('Name', 'zero-cross points');
t = (0:1./Fs:6461*Fs)';
t = t(1:size(variation_norm_mouth_smoothed_2nd_deriv_zeros, 2));
plot(t, variation_norm_mouth_smoothed_2nd_deriv_zeros(1,:), '*');
xlabel('Time (s)');
hold on;
plot(t, variation_norm_mouth_smoothed_2nd_deriv_zeros(2,:), '*');
hold on;
plot(t, variation_norm_mouth_smoothed_2nd_deriv_zeros(3,:), '*');
hold on;
plot(t, variation_norm_mouth_smoothed_2nd_deriv_zeros(4,:), '*');
hold on;
plot(t, variation_norm_mouth_smoothed_2nd_deriv_zeros(5,:), '*');
ylim([0, 6]);
xlim([0, 270]);

figure('Name', '2nd derivation');
t = (0:1./Fs:6461*Fs)';
t = t(1:size(variation_norm_mouth_smoothed_s2_2nd_deriv, 2));
plot(t, variation_norm_mouth_smoothed_s2_2nd_deriv);
xlabel('Time (s)');
ylabel('\sigma=2');
xlim([0, 270]);
figure('Name', '2nd derivation');
t = (0:1./Fs:6461*Fs)';
t = t(1:size(variation_norm_mouth_smoothed_s2_2nd_deriv, 2));
plot(t, variation_norm_mouth_smoothed_s4_2nd_deriv);
xlabel('Time (s)');
ylabel('\sigma=4');
xlim([0, 270]);
figure('Name', '2nd derivation');
t = (0:1./Fs:6461*Fs)';
t = t(1:size(variation_norm_mouth_smoothed_s2_2nd_deriv, 2));
plot(t, variation_norm_mouth_smoothed_s8_2nd_deriv);
xlabel('Time (s)');
ylabel('\sigma=8');
xlim([0, 270]);
figure('Name', '2nd derivation');
t = (0:1./Fs:6461*Fs)';
t = t(1:size(variation_norm_mouth_smoothed_s2_2nd_deriv, 2));
plot(t, variation_norm_mouth_smoothed_s16_2nd_deriv);
xlabel('Time (s)');
ylabel('\sigma=16');
xlim([0, 270]);
figure('Name', '2nd derivation');
t = (0:1./Fs:6461*Fs)';
t = t(1:size(variation_norm_mouth_smoothed_s2_2nd_deriv, 2));
plot(t, variation_norm_mouth_smoothed_s32_2nd_deriv);
xlabel('Time (s)');
ylabel('\sigma=32');
xlim([0, 270]);

% idx = label(variation_norm_mouth, variation_norm_mouth_smoothed_2nd_deriv_zeros(5,:));
idx = label(points_198(:, 1:2*num), variation_norm_mouth_smoothed_2nd_deriv_zeros(5,:));
figure(6);
ts = timeseries(idx);
ts.DataInfo.Interpolation = tsdata.interpolation('zoh');
ts.TImeInfo.Units = 'frame';
subplot(2,1,1)
plot(ts);
subplot(2,1,2);
plot(variation_norm_mouth_smoothed_s2);
