function [ newptr ] = recreseg( csegseq, currentlevel, lmaxptr, jump, signal )
    nextlevel = currentlevel+jump;
    nptrnum = length(lmaxptr{nextlevel});
    nsegseq = [vertcat(1, 1+lmaxptr{nextlevel}(1, 1:nptrnum)'),...
               vertcat(lmaxptr{nextlevel}(1, 1:nptrnum)', length(signal))];
    k=1;
    st=1;
    newptr = [];
    for i = 1:size(csegseq,1)
        if(csegseq(i,2)==nsegseq(k,2))
            cerr=0;
            ctll=0;
            cgenseqs = [];
            for j=i:-1:st
                seg{1}=signal(1,csegseq(j,1):csegseq(j,2));
                if(size(seg{1},2)<1)
                    continue
                end
                [params, tll, genseq] = easyAR(seg);
                cerr=params.terr+cerr;
                ctll=ctll+tll;
                cgenseqs = [genseq cgenseqs];
            end
            seg{1}=signal(1,nsegseq(k,1):nsegseq(k,2));
            [params, tll, ngenseqs] = easyAR(seg);
            nerr=params.terr;
            ntll=tll;
            cerr = sum(sqrt((seg{1}-cgenseqs).^2));
            nerr = sum(sqrt((seg{1}-ngenseqs).^2));
            % if(ntll > ctll)
            if(nerr < 1.2 * cerr)
                segseq = nsegseq(k,:);
            else
                segseq = csegseq(st:i, :);
            end
            newptr = [newptr; segseq];
            k=k+1;
            st=i+1;
            if(k>size(nsegseq,1))
                break
            end
        end
    end
    if(size(newptr)==[0,0])
        newptr=lmaxptr{currentlevel};
    else
        newptr = newptr(:,2)';
        newptr = newptr(1,1:length(newptr)-1);
    end

    currentlevel=currentlevel+jump;
    newptr = recreseg(newptr, currentlevel, lmaxptr, jump, signal);
end
