function [ idx ] = label( signal, points )
% 文節化済みの信号を受けとって適当にクラスタ分けする
% 切るフレームの情報がpointsに非ゼロで入ってる
delim = find(points);
signal = sum(signal,2).';
val(1,1) = mean(signal(1, 1:delim(1,1)));
for i=2:size(delim,2)
    val(1,i) = mean(signal(1, delim(1,i-1):delim(1,i)));
end

idx = kmeans(val', 8);
