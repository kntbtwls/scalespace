function [ points_xyz, points_198, points_each, points_xyzeach ] = transform(filename, num)
% 時系列データをいろいろな形に変換する
% filename: 顔座標点の時系列データファイル名
% num: 顔座標点の数

% filenameファイルの中身は、(軸)(特徴点番号)_(フレーム数)として
% x1_1 y1_1 z1_1
% x2_1 y2_1 z2_1
% ...
% x66_1 y66_1 z66_1
% x1_2 y1_2 z1_2
% ...
% x66_n y66_n z66_n
points_xyz = dlmread(filename);
s = size(points_xyz);

% 顔特徴点毎に分割
% 列はxyz
for i = 1:s(1,1)
    frame = floor(i/num)+1;
    loc = rem(i, num);

    if loc == 0
        loc = 66;
        frame = frame - 1;
    end

    points_each(frame,:,loc) = points_xyz(i,:);
end

% points_xyzを以下の形に変換
% x1_1 x2_1...x66_1 y1_1...y66_1 z1_1...z66_1
% x1_2 x2_2...x66_2 y1_2...y66_2 z1_2...z66_2
% ...
% x1_n x2_n...x66_n y1_n...y66_n z1_66...z66_n
for i = 1:s(1,1)
    frame = floor(i/num)+1;
    loc = rem(i, num);

    if loc == 0
        loc = 66;
        frame = frame - 1;
    end

    for j = 1:s(1,2)
        points_198(frame, 66*(j-1)+loc) = points_xyz(i,j);
    end
end

% points_198を顔のパーツ毎に分割
% まずはxyzで分ける
points_xyzeach = reshape(points_198, [size(points_198, 1), num, size(points_198, 2) / num]);
