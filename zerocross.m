function [ zeropoints ] = zerocross( signal )
length = size(signal, 2);
zeropoints = zeros(1, length);

for i = 1:length-1
    if sign(signal(i)) ~= sign(signal(i+1))
        zeropoints(i) = 1;
    end
end

end
