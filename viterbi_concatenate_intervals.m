function [retIset, mintotalerror, intervalerrors] = viterbi_concatenate_intervals(Iset, X, divCost, lmax)
%viterbi_concatenate_intervals
%   Input: Iset これを最適な形でつないでいく

K = size(Iset,1); % 入力区間数

delta = zeros(K, 1);
delta(1) = Inf; % for recursive calculation of Viterbi
VITERBI_INVALID_INDEX = -1;
durlist = VITERBI_INVALID_INDEX*ones(K, 1); % for trace back

%% calc all loglikelihood beforehand for efficiency
intervalerrors = calc_all_sqerrors(X, Iset, lmax); % 先にすべての誤差を計算しておく

fprintf('Start Viterbi for concatenating intervals\n (divCost = %f)\n', divCost);

debug = false; %true; % これをtrueにすると Viterbiの再帰計算をチェックできる

%% recursive calc
for k=1:K
    if(debug), fprintf('k= %d\n', k); end
    if ( mod(k,10)==0 ), fprintf('.'); end
    tau_max = min(lmax, k-1);

    % (1) recursive equation
    tmpmin = Inf;
    checkflag = 0;
    for tau=1:tau_max
        if(debug), fprintf('(tau, k-tau+1) = (%d, %d) : err= %f\n', tau, k-tau+1,...
                intervalerrors(tau, k-tau+1)); end
        % k-tau+1番目の区間から(k番目の区間まで)合計tau個の区間を結合した場合を評価
        tmpval = intervalerrors(tau, k-tau+1) + divCost + delta(k-tau);
        if (tmpval <= tmpmin)
            tmpmin = tmpval;
            mindur = tau;
            checkflag = 1;
        end
    end
    % (2) the first (single) intervals
    if ( k <= lmax )
        if(debug), fprintf('(tau, k-tau+1) = (%d, 1) : err= %f\n', k, ...
                intervalerrors(k, 1)); end
        % 1番目の区間からk個の区間を結合した場合を評価
        tmpval = intervalerrors(k, 1);
        if (tmpval <= tmpmin)
            tmpmin = tmpval;
            mindur = k;
            checkflag = 1;
        end
    end
    if(checkflag==0 && k==1)
        fprintf('# ( k= %4d )####### tmpmax = %.3e ########\n', k, tmpmin);
        fprintf('tmpval: %.3e, tmperr: %.3e\n: ', tmpval, tmperr);
    end
    delta(k) = tmpmin;
    durlist(k) = mindur;
    if(debug), fprintf('delta(%d)= %f\tdurlist(%d)= %d\n', k, tmpmin, k, mindur); end
end
fprintf('recursion done. \nStart trace back.\n')

mintotalerror = delta(K);

%% trace back
curend = K;
% 区間のindexの系列に関する区間を扱うので metaIset と呼ぶことにする
% つまり metaIsetのある行は，ある結合される区間の [結合開始区間index, 結合終了区間index]
% となっている．
metaIset = []; % result (updated interval sequene)
while (curend > 0)
  curdur = durlist(curend);
  I = [curend-curdur+1  curend];
  metaIset = [metaIset; I];
  curend = curend - curdur;
end
metaIset = sortrows(metaIset,1);
fprintf('Trace back done.\n')

% 結合された区間の始点，終点を取り出す
% つまり metaIsetから取り出した結合開始区間の始点，結合終了区間の終点を順に取り出せばよい
retIset = [];
for l=1:size(metaIset,1)
    retIset = [retIset; [Iset(metaIset(l,1),1), Iset(metaIset(l,2),2)]];
end

end
