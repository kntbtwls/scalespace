function dist = calc_distance(ci, cj, Xs, type_of_distance, manualQdiag, centerize)
%%
% TODO: remove the argument: manualQdiag
%     (set lds.Q before calling this function)

    n = size(ci.A, 1);
    if type_of_distance==0
        % MartinDistance
        if centerize
            dist = MartinDistance([ci.A ci.b; zeros(1, n) 1], [eye(n) zeros(n, 1)],...
                                  [cj.A cj.b; zeros(1, n) 1], [eye(n) zeros(n, 1)] );
        else
            dist = MartinDistance(ci.A, [], cj.A, [] );
        end

    elseif type_of_distance==1
        % Approximated KLDist (of sample data distributions)
        if (manualQdiag < 0)
            dQi = diag(ci.Q);   dQj = diag(cj.Q);
        else
            dQi = manualQdiag*ones(n,1);  dQj = manualQdiag*ones(n,1);
        end
        dist = KLDistance(Xs, ci.Iset, Xs, cj.Iset, ci.A, ci.b, dQi, cj.A, cj.b, dQj );

    elseif type_of_distance==2
        % Bayes factor by using predictive distribution
        dist = BFDistance(Xs, ci, cj);

    elseif type_of_distance==3
        % Bayes: KLdiv of two parameter distributions
        dist = KLParamDistance(ci, cj);
    end

end
