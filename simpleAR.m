function [sqerr, A, b] = simpleAR( X, b0, e0 )
%UNTITLED3 この関数の概要をここに記述
%   詳細説明をここに記述
X0 = X(:, b0:e0-1);
X1 = X(:, b0+1:e0);
m0 = mean(X0,2);
m1 = mean(X1,2);
X0c = bsxfun(@minus, X0, m0);

%d = size(X,1);
%alpha = 1e-8;
%A = X1*X0c'*inv(X0c*X0c'+alpha*eye(d,d));

A = X1*pinv(X0c);
b = m1 - A*m0;      
sqerr = norm(A*X0+repmat(b,1,size(X0,2))-X1, 'fro');
sqerr = sqerr*sqerr;

end

