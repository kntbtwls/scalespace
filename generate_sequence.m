function genseq = generate_sequence( xinit, A, b, len)
%% GENERATE_SEQUENCE
% generate a sequence with length (len) from LDS = {xinit, A, b]
% TODO: add noise term

    [n order_n] = size(A);
    order = order_n / n;

    genseq = zeros(n, len);

    % initial states (xinit = [order*n x 1])
    xinitseq = reshape(xinit, n, order);
    initlen = min(order,len); % extract len
    genseq(:, 1:initlen) = xinitseq(:, 1:initlen);

    xe = xinit;

    % last nvec
    for t=order+1:len
        genseq(:,t) = A*xe + b;
        xe(1:order_n-n) = xe(n+1:order_n); % shift
        xe(order_n-n+1:order_n) = genseq(:,t); % new state
    end

end
