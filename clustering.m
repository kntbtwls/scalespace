function [clresult] = clustering(Xs, order, Nmax, manualQdiag, algo, upper_bound, lmaxptr)
    centerize = 1;
    skip_refinement = 0;

    %% distance settings
    type_of_distance = 1; % 0:MartinDistance, 1:ApproxKL, (algo=12のみ) 2:Bayesfactor, 3:KLdiv of Param A

    %% dimensions
    Ns = size(Xs,2);
    n = size(Xs{1},1);
    totallen = zeros(1, Ns);

    for s=1:Ns
        totallen(s) = size(Xs{s},2); % dim & total length
    end

    fprintf('totallen: %d\n', totallen);
    fprintf('dim: %d\n', n);

    %%% Clustering
    %% initial segmentation (interval sequence)
    Ninit = 0;
    initIset = cell(1,Ns);

    for s=1:Ns
        initIset{s} = [lmaxptr(1, 1:size(lmaxptr, 2)-1)',...
                       lmaxptr(1, 2:size(lmaxptr, 2))'];

        for k=1:size(initIset{s}, 1)
            for sd=1:Ns % fill Iset{sd} for all possible indeces sd
                if s==sd
                    cl{Ninit+k}.Iset{sd} = initIset{s}(k, :); % initialize Iset in clusters
                else
                    cl{Ninit+k}.Iset{sd} = [];
                end
            end
        end
        Ninit = Ninit + size(initIset{s},1);
    end

    %% Initialize LDSs
    N = Ninit; % initial num of LDSs

    for i=1:N
        [cl{i}.A, cl{i}.b, cl{i}.Q, cl{i}.xinit, cl{i}.alpha, cl{i}.beta, ...
         cl{i}.gamma, cl{i}.Acov, cl{i}.Acovinv] = ...
            identify_system(Xs, cl{i}.Iset, algo, centerize, skip_refinement, upper_bound, order);
    end

    %% Calculate distance
    Dist = Inf(N,N);

    for i=1:N
        for j=i+1:N
            Dist(i,j) = calc_distance(cl{i}, cl{j}, Xs, type_of_distance, manualQdiag, centerize);
        end
    end

    clresult = cell(1, Nmax); % storage of result clusters

    %% Agglomerative hierarchical clustering
    while (N > 1)
        % find nearest pair
        [tmpmin, tmpidx] = min(Dist); % find min from each column
        [~, jmin] = min(tmpmin);
        imin = tmpidx(jmin);

        % display Dist
        if (N>=1 && N<=Nmax)
            Dist
        end

        % merge intervals
        for s=1:Ns
            cl{imin}.Iset{s} = remove_overlaps( [cl{imin}.Iset{s}; cl{jmin}.Iset{s}] );
        end

        % re-identify
        [cl{imin}.A, cl{imin}.b, cl{imin}.Q, cl{imin}.xinit, cl{imin}.alpha, cl{imin}.beta, cl{imin}.gamma, cl{imin}.Acov, cl{imin}.Acovinv] = ...
            identify_system(Xs, cl{imin}.Iset, algo, centerize, skip_refinement, upper_bound, order);

        fprintf('N=%d... (imin: %d, jmin: %d), maxabseig_A: %.4f\n', N, imin, jmin, max(abs(eig( [zeros(n*order-n, n) eye(n*order-n); cl{imin}.A] ))) );
        fprintf('        alpha: %.3e, beta: %.3e, gamma: %.3e\n', cl{imin}.alpha, cl{imin}.beta, cl{imin}.gamma);

        % calculate modified distances
        for i=1:imin-1
            Dist(i,imin) = calc_distance(cl{i}, cl{imin}, Xs, type_of_distance, manualQdiag, centerize);
        end
        for j=imin+1:N
            if (j==jmin)
                continue;
            end
            Dist(imin, j) = calc_distance(cl{imin}, cl{j}, Xs, type_of_distance, manualQdiag, centerize);
        end

        % erase elements
        cl(jmin) = [];
        Dist(jmin, :) = [];
        Dist(:, jmin) = [];

        N = N - 1;

        % store result
        if (N>=1 && N<=Nmax)
            clresult{N} = cl;
        end
    end
    disp('----------- clustering end -----------');

end
