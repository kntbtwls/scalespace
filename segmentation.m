function [ newptr ] = segmentation( cptr, nptr, signal )

    csegseq = [1+cptr(1, 1:size(cptr, 2)-1)',...
               cptr(1, 2:size(cptr, 2))'];
    csegseq(1,1)=1;
    if(size(csegseq, 1)==1)
        newptr = cxptr;
        return
    end

    nsegseq = [1+nptr(1, 1:size(nptr, 2)-1)',...
               nptr(1, 2:size(nptr, 2))'];
    nsegseq(1,1)=1;
    if(size(nsegseq, 1)==1)
        newptr = nptr;
        return
    end

    k=1;
    st=1;
    newptr=[];
    for i = 1:size(csegseq,1)
        if(csegseq(i,2)==nsegseq(k,2))
            cerr=0;
            ctll=0;
            cgenseqs = [];
            csimpleARerr = [];
            for j=i:-1:st
                seg{1}=signal(1,csegseq(j,1):csegseq(j,2));
                if(size(seg{1},2)<1)
                    continue
                end
                [params, tll, genseq] = easyAR(seg);
                cerr=params.terr+cerr;
                ctll=ctll+tll;
                cgenseqs = [genseq cgenseqs];
                csimpleARerr = [csimpleARerr, simpleAR(signal, csegseq(j,1), csegseq(j,2)) ];
            end
            seg{1}=signal(1,nsegseq(k,1):nsegseq(k,2));
            [params, tll, ngenseqs] = easyAR(seg);
            nerr=params.terr;
            ntll=tll;
            %%cerr = sum(sqrt((seg{1}-cgenseqs).^2));
            %%nerr = sum(sqrt((seg{1}-ngenseqs).^2));
            %cerr = sum((seg{1}-cgenseqs).^2);
            %nerr = sum((seg{1}-ngenseqs).^2);

            cerr = sum(csimpleARerr);
            nerr = simpleAR(signal, nsegseq(k,1), nsegseq(k,2));

            fprintf('--------- cerr, nerr = (%f, %f)-----------\n', cerr, nerr);

            % figure('Name', 'genseg')
            % subplot(311)
            % plot(seg{1})
            % subplot(312)
            % plot(cgenseqs)
            % subplot(313)
            % plot(ngenseqs)
            % if(ntll > ctll)
            if(nerr < 1*(i-st+1) + cerr)
                segseq = nsegseq(k,:);
            else
                segseq = csegseq(st:i, :);
            end
            newptr = [newptr; segseq];
            k=k+1;
            st=i+1;
            if(k>size(nsegseq,1))
                break
            end
        end
    end
    newptr = [1; newptr(:,2)]';


end
