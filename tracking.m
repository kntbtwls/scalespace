function [ lmaxptr ] = tracking( startlevel, endlevel, lmax )
lmaxlist=find(lmax(startlevel,:)>0); % 一番上のscale で lmax==1 と
                                     % なるインデックス(時刻)を見つける
lmaxtrace=ones(size(lmaxlist)); % どのスケールまでトレースできたか
                                % の記録用
lmaxptr=zeros(size(lmaxlist));  % 最終出力

for l=1:length(lmaxlist)    % 最大scaleでlmax==1であった各点に関して
    lmaxptr(l)=lmaxlist(l); % 最大スケールからスタート
    trackflag=true;

    for s=startlevel-1:-1:endlevel % 最大スケールから順に下へ
        clmaxlist=find(lmax(s,:)>0); % scale=s における変曲点の時刻
        nlmaxlist=find(lmax(s+1,:)>0); % scale=s+1における変曲点の時刻
        [m,idx]=min(abs(clmaxlist-lmaxptr(l))); % 時間軸上でlmaxptrと近い点をclmaxlistから見つける
        [m2,idx2]=min(abs(nlmaxlist-clmaxlist(idx))); % 時間軸上でclmaxlist(idx)と近い点をnlmaxlistからみつける

        if(lmaxptr(l)==nlmaxlist(idx2)&&trackflag) % lmaxptr->clmaxlist->nlmaxlistと近いものを辿って一致した場合
            lmaxtrace(l)=s;
            lmaxptr(l)=clmaxlist(idx); % lmaxptr は 現在のスケールでのトレース結果とする
        else
            trackflag=false; % トレース終了
        end

    end

end
