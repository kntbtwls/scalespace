function [ params, totalloglike, genseq ] = easyAR( Xtmp )
    alpha=1e-5;
    maxalpha=1e2;
    qthresh=1e1;
    evalQ=1e12;

    Nseq=length(Xtmp);

    X1=Xtmp{1}(:,1:end-1);
    xinit=Xtmp{1}(:,1);
    len=size(Xtmp{1},2);

    for i=2:Nseq
        X1=[X1,Xtmp{i}(:,1:end-1)];
        xinit=[xinit,Xtmp{i}(:,1)];
        len=[len size(Xtmp{i},2)];
    end

    X1=[X1;ones(1,size(X1,2))];
    X2=Xtmp{1}(:,2:end);

    for i=2:Nseq
        X2=[X2,Xtmp{i}(:,2:end)];
    end

    X2=[X2;ones(1,size(X2,2))];

    while(evalQ>qthresh&&alpha<maxalpha)
        alpha=alpha*10;
        % ridge regression model
        A=X2*((X1'*X1+eye(size(X1,2))*alpha)\X1');
        % diag model
        params.A=A;

        params.xinit=xinit;
        params.len=len;

        error=cell(1,Nseq);
        for i=1:Nseq
            genseq=easyARgen(A, xinit(:,i), len(i));
            error{i}=sqrt((genseq-Xtmp{i}).^2);
        end

        error=cell2mat(error);
        [mu,qdiag]=normfit(error');
        Q=zeros(length(qdiag));
        for i=1:length(qdiag)
            Q(i,i)=qdiag(i);
        end

        totalloglike=-1*sum(log(mvnpdf(error',mu,Q)+1e-7));

        params.Q=Q;
        a=A(1:end-1,:);
        params.all=[a(:);xinit(:);diag(Q); len(:)];
        params.tll=totalloglike;
        params.terr=norm(error(:));

        evalQ=norm(Q);

    end

    if(alpha>maxalpha)
        disp('----------------- alpha reached the maximum -------------');
    end

end
