points = dlmread('uekiQ2_3DShape_faceTracker_2.txt', '');
num = 66;
s = size(points);
l = s(1,1);

for j = 1:s(1,2)
    for i = 1:l-num
        V(i,j) = points(i+num,j) - points(i,j);
    end
end

Vs = size(V);

for i = 1:Vs(1,1)
    V2(i) = sqrt(abs(V(i,1))^2 + abs(V(i,2))^2);
end

V2s = size(V2);

for i = 1:V2s(1,2)
    q = floor(i/num);
    r = rem(i,num);
    V3(q+1,r+1) = V2(i);
end

V3s = size(V3);

jaw = 1:17;
eyebrow_r = 18:22;
eyebrow_l = 23:27;
nose_line = 28:31;
nose_under = 32:36;
eye_r = 37:42;
eye_l = 43:48;
mouth = 49:66;

V4=zeros(V3s(1,1),8);
for i = 1:V3s(1,1)
    for j = jaw
        V4(i,1) = V4(i,1) + V3(i,j);
    end
end
V4(:,1) = V4(:,1) / 17;

for i = 1:V3s(1,1)
    for j = eyebrow_r
        V4(i,2) = V4(i,2) + V3(i,j);
    end
end
V4(:,2) = V4(:,2) / 5;

for i = 1:V3s(1,1)
    for j = eyebrow_l
        V4(i,3) = V4(i,3) + V3(i,j);
    end
end
V4(:,3) = V4(:,3) / 5;

for i = 1:V3s(1,1)
    for j = nose_line
        V4(i,4) = V4(i,4) + V3(i,j);
    end
end
V4(:,4) = V4(:,4)/4;

for i = 1:V3s(1,1)
    for j = nose_under
        V4(i,5) = V4(i,5) + V3(i,j);
    end
end
V4(:,5) = V4(:,5) / 5;

for i = 1:V3s(1,1)
    for j = eye_r
        V4(i,6) = V4(i,6) + V3(i,j);
    end
end
V4(:,6) = V4(:,6) / 6;

for i = 1:V3s(1,1)
    for j = eye_l
        V4(i,7) = V4(i,7) + V3(i,j);
    end
end
V4(:,7) = V4(:,7) / 6;

for i = 1:V3s(1,1)
    for j = mouth
        V4(i,8) = V4(i,8) + V3(i,j);
    end
end
V4(:,8) = V4(:,8) / 18;
