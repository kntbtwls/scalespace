% 解析テスト

clear all
close all

% デフォルトの色を 2014a 以前に戻す
co = [0 0 1;
      0 0.5 0;
      1 0 0;
      0 0.75 0.75;
      0.75 0 0.75;
      0.75 0.75 0;
      0.25 0.25 0.25];
set(groot,'defaultAxesColorOrder',co)

analysisrange = [
 225,275
 870,920
 1550,1600
 2330,2380
 2860,2910
 3540,3590 % 発話
 3920,3970
 4170,4220
 4440,4490
 5160,5210
];

%
plot_facepoints = false;
plot_intensity = false;

%seqname = 'taharaQ2';
seqname = 'uekiQ1';
% uekiQ1, uekiQ2, taharaQ2
datadir = 'SignalProcessing';
load( sprintf('../%s/%sX.mat', datadir, seqname));
load( sprintf('../%s/%sY.mat', datadir, seqname));

load( sprintf('../%s/%sint.mat', datadir, seqname));

% sample周期 : 8 msec = 125fps
totalframe = 6461;
totalsec = 270;
fps = totalframe / totalsec;

%{
% Audio data resampling
adatadir = 'txtDATA';
intensity = dlmread( sprintf('../%s/%sint.txt', adatadir, seqname), '\t', 1, 1);
if(plot_intensity)
    figure('Name', 'intensity')
    plot(intensity(:,1), intensity(:,2));
end

resampled_intensity = resample(intensity, totalframe, size(intensity,1));
intensity = resampled_intensity;
%}

%fps = 23.93;
%--- 特徴点の番号 -------------------------------------------------
jaw = 1:17;
eyebrow_r = 18:22;
eyebrow_l = 23:27;
nose_line = 28:31;
nose_under = 32:36;
eye_r = 37:42;
eye_l = 43:48;
mouth = 49:66;

mouth_edge = [49,55]; % 唇の端点 (x見る)
lip_ul = [52, 58]; % 上下唇 (y見る)
eye_r_ul = [38,39,41,42]; % 右目上下
eye_l_ul = [44,45,47,48]; % 左目上下

eyebrow_inner = [22,23]; % 眉間
eyebrow_r_ul = [18,20,22]; %

%--- 平滑化 ---------------------------------------------------------
use_smoothing = true;   % 平滑化するかどうか
sigma1 = 2; % ガウシアンの標準偏差 [frame]
sigma2 = 1; % DoG用

ws1 = 6*sigma1 + 1; % Size of the averaging window
% gaussian ( max at x=3sigma+1, x=k and x=6sigma+1 - (k-1) take same value)
%window = ones(ws,1)/ws;
window1 = exp(- ((1:ws1)' - (3*sigma1+1)).^2./(sigma1^2)); % 6sigmaが入るぐらい
window1 = window1 / sum(window1); % 窓を計算
s1AfX = convn( AfX , window1, 'same');
s1AfY = convn( AfY , window1, 'same');
s1Intensity = convn( intensity, window1, 'same');

% DoG用
ws2 = 6*sigma2 + 1; % Size of the averaging window
% gaussian ( max at x=3sigma+1, x=k and x=6sigma+1 - (k-1) take same value)
%window = ones(ws,1)/ws;
window2 = exp(- ((1:ws2)' - (3*sigma2+1)).^2./(sigma2^2)); % 6sigmaが入るぐらい
window2 = window2 / sum(window2); % 窓を計算
s2AfX = convn( AfX , window2, 'same');
s2AfY = convn( AfY , window2, 'same');
s2Intensity = convn( intensity, window2, 'same');

if(use_smoothing) % 平滑化する場合
    datX = s1AfX;
    datY = s1AfY;
    audioint = s1Intensity;
else
    datX = AfX;
    datY = AfY;
    audioint = intensity;
end

%% 平均とスケールを正規化
use_normalization = false;

if (use_normalization)
    datX(:, mouth) = normalize_by_max( datX(:, mouth), 1 );
    datY(:, mouth) = normalize_by_max( datY(:, mouth), 1 );
end



%% 特徴点を2次元プロット

if(plot_facepoints)
    figure('Name', 'face (after normalization)');
    t=1;
    plot(0, 0, 'o');
    text(0, 0, 'origin');
    hold on
    for i=1:66
        plot(datX(t, i), datY(t, i), '.');
        text(datX(t, i), datY(t, i), sprintf('%d', i));
        axis ij
    end
    hold off
end


%% 特徴点やintensityをプロット
figure('Name', 'feature plot', 'Position', [100, 200, 1600, 800]);
%Np = 5;
Np = 4;

h1 = subplot(Np, 1, 1); plot(datY(:, eye_r_ul(4))), plotlines(gca,analysisrange,'r'), ylabel('Y42 (right eye under)'), grid;
h2 = subplot(Np, 1, 2); plot(datX(:, mouth_edge(1))), plotlines(gca,analysisrange,'r'), ylabel('X49 (mouth edge)'), grid;
%h2 = subplot(Np, 1, 2); plot(datX(:, mouth_edge)), plotlines(gca,analysisrange,'r'), ylabel('X49,55 (mouth edge)'), grid;
h3 = subplot(Np, 1, 3); plot(datY(:, lip_ul(2))), plotlines(gca,analysisrange,'r'), ylabel('Y58 (lip under)'), grid;
%h3 = subplot(Np, 1, 3); plot(datY(:, lip_ul)), plotlines(gca,analysisrange,'r'), ylabel('Y58,52 (lip under,upper)'), grid;
h4 = subplot(Np, 1, 4); plot(audioint), plotlines(gca,analysisrange,'r'), ylabel('Audio intensity'), grid;
%h5 = subplot(Np, 1, 5); plot(datY(:, mouth)), ylabel('Y49-66 (mouth)'), grid;
tmpdat = datY(:,58) - datY(:,52);
%tmpdat = s2AfY - s1AfY; % DoG

%h5 = subplot(Np, 1, 5); plot(tmpdat(:,58)), ylabel('tmpdat'), grid;
%h5 = subplot(Np, 1, 5); plot(tmpdat), plotlines(gca,analysisrange,'r'),
%ylabel('tmpdat'), grid; % 口の形状

 %一部の時区間について
%Wsize = 500;
Wsize = 4000; % 全体プロット用
%Wsize = 5500;
%scrollsubplot(Wsize, 200:totalframe, h1, h2, h3, h4, h5);
scrollsubplot(Wsize, 100:totalframe, h1, h2, h3, h4); % 全体プロット用
%scrollsubplot(Wsize, (885-Wsize/2):totalframe, h1, h2, h3, h4); %signal2
%scrollsubplot(Wsize, (3945-Wsize/2):totalframe, h1, h2, h3, h4); %signal kizuki

%%
maxint = max(audioint);
medint = median(audioint);
relative_int = zeros(1, length(analysisrange));
for i=1:length(analysisrange)
    roi = analysisrange(i,:);
    relative_int(i) = (max(audioint(roi(1):roi(2)))-medint)/(maxint-medint);
end

save('relative_int.mat', 'relative_int', 'analysisrange')
%{
%% パーツごとに特徴点やintensityをプロット
figure('Name', 'parts plot', 'Position', [300, 100, 1600, 800]);
Np = 5;
h1 = subplot(Np, 1, 1); plot(datY(:, eye_r_ul)), ylabel('eye r ul (Y)'), grid;
h2 = subplot(Np, 1, 2); plot(datY(:, lip_ul)), ylabel('lip ul (Y)'), grid;
h3 = subplot(Np, 1, 3); plot(datY(:, mouth)), ylabel('mouth (Y)'), grid;
h4 = subplot(Np, 1, 4); plot(datX(:, mouth)), ylabel('mouth (X)'), grid;
h5 = subplot(Np, 1, 5); plot(audioint), ylabel('Audio intensity'), grid;

scrollsubplot(500, 200:totalframe, h1, h2, h3, h4, h5);
%}

%{
%% 一部切り出してLDS求めてみる

figure('Name', 'Test Eigenvalue');
begframe = 1575;
interval = [begframe, begframe+20];
t_range = interval(1):interval(2);
X = [datX(t_range, mouth_edge(1)), datY(t_range, lip_ul(2))]';
%X = datX(t_range, mouth_edge(1))';
%X = datY(t_range, lip_ul(2))';
subplot(311), plot(t_range,  X);
ds = estimateDS(X);
lambda = eig(ds.A)
abslambda = abs(lambda)
[err_d, err_i] = calcerror_byDS(X, ds)

genX = evolveDS(ds, length(t_range));
subplot(312), plot(t_range, genX');

% dynamic range の大きい方が誤差は大きくなるため，比較するならどう正規化するか?
% ...
%}

return
%% 二つのLDSがあると仮定して分節化してみる
I = [230, 280];
%I = [870, 900];
%I = [1550, 1590];
%I = [3930, 3970];
%I = [2300, 2400];
offset = 5; % dim より十分大きいこと
%X = [datX(:, mouth_edge(1)), datY(:, lip_ul(2))]';
%X = datX(:, mouth_edge(1))';
%X = datY(:, lip_ul(2))';
X = audioint';

prederr = [];
Ibeg = (I(1)+offset);
Iend = (I(2)-offset);
for mid = Ibeg : Iend
    X1 = X(:, I(1):mid);
    X2 = X(:, (mid+1):I(2));
    ds1 = estimateDS( X1 );
    [err1d, err1i] = calcerror_byDS( X1, ds1 );

    ds2 = estimateDS( X2 );
    [err2d, err2i] = calcerror_byDS( X2, ds2 );

    prederr = [prederr; err1d, err1i, err2d, err2i];
end

%
figure('Name', 'segmentation')
t_range = I(1):I(2);
t_range_offset = Ibeg:Iend;

h1 = subplot(211); plot( t_range, X( :, t_range )' ); title('X'); % 元データを表示

sumerr = sum(prederr, 2);
[dummy, min_t] = min(sumerr);
min_t = min_t + Ibeg - 1;
hold on, plot( [min_t, min_t], get(gca, 'ylim'), 'r'), hold off % 予測誤差最小の点を表示
title(sprintf('Input sequence & min t (= %d)', min_t));

% 予測誤差を表示
h2 = subplot(212); plot( t_range_offset, [prederr, sumerr]); title('prederr'); xlim(I);


return

%% ためしに固有値を計算してみる
X = cell(1,1);
biaslist = 0:5:100;
eigvals = zeros(3, length(biaslist));

col = 1;
for bias = biaslist
    t_range = (650:660) + bias;
    X{1} = datY(t_range, 58)';

    ds = estimateDS(X);
    % ds.A
    % ds.b
    % ds.x0
    lambda = eig(ds.A);
    abslambda = abs(lambda);
    %sort(abs(lambda))
    eigvals(1, col) = max(abslambda);
    eigvals(2, col) = min(abslambda);

    genX = evolveDS(ds, length(t_range));
    %{
    figure('Name', 'genX');
    subplot(211), plot(genX')
    subplot(212), plot(X{1}')
    %}
    % intensity
%{
    X{1} = audioint(t_range)';

    ds = estimateDS(X);
    %{
    disp('A')
    ds.A
    disp('b')
    ds.b
    disp('sorted abs(lambda)')
    %}
    lambda = eig(ds.A);
    eigvals(3, col) = lambda;
    %sort(abs(lambda))
%}
    col = col + 1;
end
figure(99)
plot(eigvals')

%ts = 2500:5000;
%scrollsubplot704(3, ts, [datX(ts, mouth_edge), datY(ts, lip_ul), datY(ts, mouth)], [], 'on')

%subplot(Np, 1, 3), plot(datX(:,
