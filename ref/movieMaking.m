%% the first part: making the video of 3D shape from faceTracker.
folder='../data/';
fileNames={'uekiQ1'};
for i=1:length(fileNames)
    shapefile=[folder fileNames{i} '_3DShape_faceTracker.txt'];
    parameterfile=[folder fileNames{i} '_shapeParameter_faceTracker.txt'];
    VideoTrackingResult=[folder fileNames{i} '_tracking_faceTracker.avi'];
    saveVideo=[folder fileNames{i} '_3DShapeAndTrackResult'];
    visualizeFaceTracking(shapefile, parameterfile, VideoTrackingResult, saveVideo);
end

% %% The second part: This one is used for visulaization (plot 2 videos on a window), one the face tracking result, another one the
% Video1='C:\Users\zhang\Downloads\chehra_v0.1\Chehra_v0.1\Chehra\Chehra\test\UekiQ2_3DShape.avi';
% Video2='C:\Users\zhang\Downloads\chehra_v0.1\Chehra_v0.1\Chehra\Chehra\test\UekiQ2_tracking_faceTracker.avi';
%
% shuttleVideo1 = VideoReader(Video1);
% shuttleVideo2 = VideoReader(Video2);
%
% % Set up the video .
% writerObj = VideoWriter('C:\Users\zhang\Downloads\chehra_v0.1\Chehra_v0.1\Chehra\Chehra\test\UekiQ2_3DShapeAndTrackingResult.avi'); % Name it.
% writerObj.FrameRate = 10; % How many frames per second.
% open(writerObj);
%
% for i=1:shuttleVideo1.NumberOfFrames
%     imgLeft = read(shuttleVideo1,i);
%     imgRight =read(shuttleVideo2,i);
% %    imgLeft = readFrame(shuttleVideo1);
% %    imgRight = readFrame(shuttleVideo2);
%    figure(1);
%    subplot(1,2,1);imshow(imgLeft);
%    subplot(1,2,2);imshow(imgRight);
%    frame = getframe(gcf); % 'gcf' can handle if you zoom in to take a movie.
%    writeVideo(writerObj, frame);
% end
