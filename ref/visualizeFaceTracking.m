function visualizeFaceTracking(shapefile, parameterfile, VideoTrackingResult, saveVideo)
% reading the shape file
fileID = fopen(shapefile,'r');
formatSpec = '%f %f %f';
sizeA = [3 Inf];
shapeData = fscanf(fileID,formatSpec,sizeA);
fclose(fileID);
% reading the parameter file
fileID = fopen(parameterfile,'r');
formatSpec = '%f %f %f';
sizeA = [6 Inf];
parameterData = fscanf(fileID,formatSpec,sizeA);
fclose(fileID);

% [Xi,Yi,Zi] = textread(datafile, '%f %f %f',-1);
Xi=shapeData(1, :);
Yi=shapeData(2, :);
Zi=shapeData(3, :);

close all;
nLandmarks=66;
nFrames=length(Xi)/nLandmarks;

%% Set up the movie.
writerObj = VideoWriter(saveVideo); % Name it.
writerObj.FrameRate = 20; % How many frames per second.
open(writerObj); 
shuttleVideo = VideoReader(VideoTrackingResult);
%face detector
faceDetector = vision.CascadeObjectDetector();
scrsz = get(groot,'ScreenSize');
figure('Position',[1 1 scrsz(3)*3/4 scrsz(4)*3/4])
 
for i=1:nFrames      
    % We just use pause but pretend you have some really complicated thing here...
    pause(0.1);
    figure(1); % Makes sure you use your desired frame.
%    axis([-30 20 -20 30 -15 25]);
    X=Xi(((i-1)*nLandmarks+1):i*nLandmarks);
    Y=Yi(((i-1)*nLandmarks+1):i*nLandmarks);
    Z=Zi(((i-1)*nLandmarks+1):i*nLandmarks);
   % scatter3(X,Y,Z);
   subplot(1,2,1);
   scatter(X,Y);
   axis equal;
   axis ij;
   xlim([-30,30])
   ylim([-30,30])
   parameters=parameterData(:,i);
   strmin = ['Pitch=', num2str(parameters(2)), ...
       ' Yaw=', num2str(parameters(3)),' Roll=', num2str(parameters(4)), char(13,10)',...
       'Scale = ',num2str(parameters(1)), ' Tx=', num2str(parameters(5)), ' Ty=', num2str(parameters(6))];
   
   text(-25,-25,strmin,'HorizontalAlignment','left');
   trackingResult=read(shuttleVideo,i);
   %bbox = step(faceDetector, trackingResult)
   faceImage=trackingResult(250:650,450:850,:);
   subplot(1,2,2);imshow(faceImage);
   frame = getframe(gcf); % 'gcf' can handle if you zoom in to take a movie.
   writeVideo(writerObj, frame);
 
end
close(writerObj); % Saves the movie.
end